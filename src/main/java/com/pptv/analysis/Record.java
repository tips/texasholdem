package com.pptv.analysis;

import java.util.Date;

public class Record {

	private Integer id;

	private String board;

	private String cowboy;

	private String calf;

	private String handtype;

	private String wintype;

	private String who;

	private Date time;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getCowboy() {
		return cowboy;
	}

	public void setCowboy(String cowboy) {
		this.cowboy = cowboy;
	}

	public String getCalf() {
		return calf;
	}

	public void setCalf(String calf) {
		this.calf = calf;
	}

	public String getHandtype() {
		return handtype;
	}

	public void setHandtype(String handtype) {
		this.handtype = handtype;
	}

	public String getWintype() {
		return wintype;
	}

	public void setWintype(String wintype) {
		this.wintype = wintype;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "公共牌：" + board + "\n牛仔手牌：" + cowboy + "\n小牛手牌：" + calf + "\n手牌牌型：" + handtype + "\n胜负关系：" + who
				+ "\n获胜牌型：" + wintype;
	}
}
