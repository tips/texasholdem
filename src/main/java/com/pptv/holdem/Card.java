package com.pptv.holdem;

public class Card implements Comparable<Card> {

	/* 13种牌面 */
	public final static int CODE_NUM = 13;

	/* 4种花色 */
	public final static int SUIT_NUM = 4;

	public final static String[] CODE_NAME = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
	public final static String[] SUIT_NAME = { "Diamond", "Club", "Heart", "Spade" };// 方片、梅花、红桃、黑桃

	public final static int DIAMONDS = 0;
	public final static int CLUBS = 1;
	public final static int HEARTS = 2;
	public final static int SPQDES = 3;

	/**
	 * 当前牌在52张中的序号
	 */
	private int serial;

	public Card(int serial) {
		this.setSerial(serial);
	}

	/**
	 * 获得牌面值对应的code
	 * 
	 * @param serial
	 * @return 返回CODE_NAME数组的下标
	 */
	public static int code(int serial) {
		return (serial / SUIT_NUM);
	}

	/**
	 * 根据code获取牌面值
	 * 
	 * @param code
	 * @return
	 */
	public static String codeName(int code) {
		return CODE_NAME[code];
	}

	/**
	 * 获得花色code
	 * 
	 * @param serial
	 * @return
	 */
	public static int suit(int serial) {
		return (serial % SUIT_NUM);
	}

	/**
	 * 获得花色
	 * 
	 * @param code
	 * @return
	 */
	public static String suitName(int code) {
		return SUIT_NAME[code];
	}

	/**
	 * 获得牌顺序值
	 * 
	 * @param code
	 * @param suit
	 * @return
	 */
	public static int serial(int code, int suit) {
		return (code * Card.SUIT_NUM + suit);
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	@Override
	public int compareTo(Card o) {
		return Card.code(this.getSerial()) - Card.code(o.getSerial());
	}

	@Override
	public String toString() {
		return suitName(suit(serial)) + "#" + codeName(code(serial));
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Card))
			return false;
		Card tmp = (Card) o;
		return Card.code(this.getSerial()) == Card.code(tmp.getSerial());
	}
}
