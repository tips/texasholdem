package com.pptv.holdem;

import java.util.Collections;
import java.util.List;

/**
 * 牌型结果类
 * 
 * @author longhu
 *
 */
public class Result {

	private int type; // 结果的类型，例如高牌0，对子1....皇家同花顺9

	private Card card1;

	private Card card2;

	/**
	 * 排好序的
	 */
	private List<Card> cards;
	
	public Result() {
	}

	public Result(int type) {
		this.type = type;
	}

	public Result(int type, Card card1) {
		this.type = type;
		this.card1 = card1;
	}

	public Result(int type, List<Card> cards) {
		this.type = type;
		this.cards = cards;
		Collections.sort(this.cards);
	}

	public Result(int type, Card card1, Card card2) {
		this.type = type;
		this.card1 = card1;
		this.card2 = card2;
	}

	public Result(int type, Card card1, List<Card> cards) {
		this.type = type;
		this.card1 = card1;
		this.cards = cards;
		Collections.sort(this.cards);
	}

	public Result(int type, Card card1, Card card2, List<Card> cards) {
		this.type = type;
		this.card1 = card1;
		this.card2 = card2;
		this.cards = cards;
		Collections.sort(this.cards);
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Card getCard1() {
		return card1;
	}

	public void setCard1(Card card1) {
		this.card1 = card1;
	}

	public Card getCard2() {
		return card2;
	}

	public void setCard2(Card card2) {
		this.card2 = card2;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	/**
	 * 比较大小
	 * @param o
	 * @return
	 */
	public int compareTo(Result o) {
		if (this.getType() > o.getType())
			return 1;
		else if (this.getType() < o.getType())
			return -1;
		else {
			// 高牌
			if (this.getType() == 0) {
				for (int i = 4; i >= 0; i--) {
					if (Card.code(this.getCards().get(i).getSerial()) > Card.code(o.getCards().get(i).getSerial()))
						return 1;
					else if (Card.code(this.getCards().get(i).getSerial()) < Card.code(o.getCards().get(i).getSerial()))
						return -1;
				}
				return 0;
			}
			// 对子
			else if (this.getType() == 1) {
				if (this.getCard1().compareTo(o.getCard1()) > 0) {
					return 1;
				} else if (this.getCard1().compareTo(o.getCard1()) < 0) {
					return -1;
				} else {
					for (int i = 2; i >= 0; i--) {
						if (Card.code(this.getCards().get(i).getSerial()) > Card.code(o.getCards().get(i).getSerial()))
							return 1;
						else if (Card.code(this.getCards().get(i).getSerial()) < Card
								.code(o.getCards().get(i).getSerial()))
							return -1;
					}
					return 0;
				}
			}
			// 两对
			else if (this.getType() == 2) {
				if (this.getCard1().compareTo(o.getCard1()) > 0) {
					return 1;
				} else if (this.getCard1().compareTo(o.getCard1()) < 0) {
					return -1;
				} else {
					if (this.getCard2().compareTo(o.getCard2()) > 0) {
						return 1;
					} else if (this.getCard2().compareTo(o.getCard2()) < 0) {
						return -1;
					} else {
						if (Card.code(this.cards.get(0).getSerial()) > Card.code(this.cards.get(0).getSerial()))
							return 1;
						else if (Card.code(this.cards.get(0).getSerial()) < Card.code(this.cards.get(0).getSerial()))
							return -1;
						else
							return 0;
					}
				}
			}

			// 三条
			else if (this.getType() == 3) {
				if (this.getCard1().compareTo(o.getCard1()) > 0) {
					return 1;
				} else if (this.getCard1().compareTo(o.getCard1()) < 0) {
					return -1;
				} else {
					for (int i = 1; i >=0; i--) {
						if (Card.code(this.getCards().get(i).getSerial()) > Card.code(o.getCards().get(i).getSerial()))
							return 1;
						else if (Card.code(this.getCards().get(i).getSerial()) < Card
								.code(o.getCards().get(i).getSerial()))
							return -1;
					}
					return 0;
				}

			}

			// 顺子
			else if (this.getType() == 4) {
				if (this.getCard1().compareTo(o.getCard1()) > 0) {
					return 1;
				} else if (this.getCard1().compareTo(o.getCard1()) < 0) {
					return -1;
				} else
					return 0;
			}

			// 同花
			else if (this.getType() == 5) {
				for (int i = 4; i >= 0; i--) {
					if (Card.code(this.getCards().get(i).getSerial()) > Card.code(o.getCards().get(i).getSerial()))
						return 1;
					else if (Card.code(this.getCards().get(i).getSerial()) < Card.code(o.getCards().get(i).getSerial()))
						return -1;
				}
				return 0;
			}

			// 葫芦
			else if (this.getType() == 6) {
				if (this.getCard1().compareTo(o.getCard1()) > 0)
					return 1;
				else if (this.getCard1().compareTo(o.getCard1()) < 0)
					return -1;
				else {
					if (this.getCard2().compareTo(o.getCard2()) > 0)
						return 1;
					else if (this.getCard2().compareTo(o.getCard2()) < 0)
						return -1;
				}
				return 0;
			}

			// 四条
			else if (this.getType() == 7) {
				if (this.getCard1().compareTo(o.getCard1()) > 0) {
					return 1;
				} else if (this.getCard1().compareTo(o.getCard1()) < 0) {
					return -1;
				} else {
					if (Card.code(this.cards.get(0).getSerial()) > Card.code(this.cards.get(0).getSerial()))
						return 1;
					else if (Card.code(this.cards.get(0).getSerial()) < Card.code(this.cards.get(0).getSerial()))
						return -1;
					else
						return 0;
				}

			}
			// 同花顺
			else if (this.getType() == 8) {
				if (this.getCard1().compareTo(o.getCard1()) > 0) {
					return 1;
				} else if (this.getCard1().compareTo(o.getCard1()) < 0) {
					return -1;
				} else
					return 0;
			} else//else 分支即皇家同花顺，所有的皇家同花顺都一样大
				return 0;
		}
	}

}
