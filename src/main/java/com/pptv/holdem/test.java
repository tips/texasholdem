package com.pptv.holdem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pptv.analysis.Record;
import com.pptv.calculator.Calculator;
import com.pptv.calculator.MaxTypeCal;

import com.pptv.utils.DBUtil;

public class test {
	public static void main(String[] args) {
		Result cowboy = new Result();
		Result calf = new Result();
		Record record = new Record();
		long i = 1000000;
		while (i>0) {
			try {
//				System.out.println("洗牌...");
				Thread.sleep(1);
				Poker.shuffle();
			} catch (InterruptedException e) {

			}
			cowboy = MaxTypeCal.classify(Poker.cowboyUnion());
			calf = MaxTypeCal.classify(Poker.calfUnion());
			record.setBoard(Poker.board().toString());
			record.setCowboy(Poker.cowboyHand().toString());
			record.setCalf(Poker.calfHand().toString());
			record.setHandtype(Calculator.handType(Poker.cowboyHand(), Poker.calfHand()).toString());
			record.setWho(Calculator.whoWin(cowboy, calf));
			record.setWintype( Calculator.winType(cowboy, calf));
//			System.out.println(record.toString());
//			System.out.println("------------------------------------------------");
			DBUtil.insert(record);
			i--;
		}
	}
}
