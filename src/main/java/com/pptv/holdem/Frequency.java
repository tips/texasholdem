package com.pptv.holdem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * code出现次数
 * @author longhu
 *
 */
public class Frequency {
	
	private int code;
	private int time;

	public Frequency(int code,int time){
		this.code = code;
		this.time = time;
	}
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}
	
	/**
	 * 返回cards中重复牌的code和重复次数
	 * 
	 * @param cards
	 * @return
	 */
	public static List<Frequency> repetition(List<Card> cards) {
		List<Frequency> frcs = new ArrayList<Frequency>();
		int time = Collections.frequency(cards, new Card(0));
		if (time > 1) {
			Frequency frc = new Frequency(0, time);
			frcs.add(frc);
		}
		
		time = Collections.frequency(cards, new Card(4));
		if (time > 1) {
			Frequency frc = new Frequency(1, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(8));
		if (time > 1) {
			Frequency frc = new Frequency(2, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(12));
		if (time > 1) {
			Frequency frc = new Frequency(3, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(16));
		if (time > 1) {
			Frequency frc = new Frequency(4, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(20));
		if (time > 1) {
			Frequency frc = new Frequency(5, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(24));
		if (time > 1) {
			Frequency frc = new Frequency(6, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(28));
		if (time > 1) {
			Frequency frc = new Frequency(7, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(32));
		if (time > 1) {
			Frequency frc = new Frequency(8, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(36));
		if (time > 1) {
			Frequency frc = new Frequency(9, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(40));
		if (time > 1) {
			Frequency frc = new Frequency(10, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(44));
		if (time > 1) {
			Frequency frc = new Frequency(11, time);
			frcs.add(frc);
		}

		time = Collections.frequency(cards, new Card(48));
		if (time > 1) {
			Frequency frc = new Frequency(12, time);
			frcs.add(frc);
		}
		return frcs;
	}
}
