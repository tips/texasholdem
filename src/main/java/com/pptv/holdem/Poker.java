package com.pptv.holdem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Poker {

	/* 52张牌 */
	public static final int CARD_NUM = 52;

	public static final String[] cardType = { "high card", "one pair", "two pair", "three of a kind", "straight",
			"flush", "full house", "four of a kind", "straight flush", "royal flush" };

	/**
	 * 存放牌的容器
	 */
	public static final List<Card> container = new ArrayList<Card>();

	/**
	 * 初始化52张扑克，从0到51
	 */
	static {
		for (int i = 0; i < Poker.CARD_NUM; i++) {
			container.add(new Card(i));
		}
	}

	/**
	 * 洗牌，每局都要洗牌
	 */
	public static void shuffle() {
		Collections.shuffle(container);
	}

	/**
	 * 牛仔手牌，2张
	 * 
	 * @return
	 */
	public static List<Card> cowboyHand() {
		return Poker.container.subList(0, 2);
	}

	/**
	 * 小牛手牌，2张
	 * 
	 * @return
	 */
	public static List<Card> calfHand() {
		return Poker.container.subList(7, 9);
	}

	/**
	 * 公共牌
	 * 
	 * @return
	 */
	public static List<Card> board() {
		return Poker.container.subList(2, 7);
	}

	/**
	 * 返回牛仔的7张牌
	 * 
	 * @return
	 */
	public static List<Card> cowboyUnion() {
		return Poker.container.subList(0, 7);
	}

	/**
	 * 返回小牛的7张牌
	 * 
	 * @return
	 */
	public static List<Card> calfUnion() {
		return Poker.container.subList(2, 9);
	}
}
