package com.pptv.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pptv.holdem.Card;
import com.pptv.holdem.Frequency;
import com.pptv.holdem.Result;

public class MaxTypeCal implements CalculatorInterface {


	public static Result classify(List<Card> map) {
		Map<Integer, List<Card>> group = sevenToFive(map);// 先对7张牌分组，共21种情况
		Result result = new Result(-1);
		for (List<Card> cards : group.values()) {// 分别处理21个数组

			// 皇家同花顺
			if (result.getType() < 9) {
				Result tmp = king(cards);
				if (tmp != null) {
					result = tmp;
					continue;
				}
			}
			// 同花顺
			if (result.getType() <= 8) {
				Result tmp = suitSerial(cards);
				if (tmp != null) {
					if (result.getType() < 8)
						result = tmp;
					else if (result.getType() == 8 && result.compareTo(tmp) < 0)
						result = tmp;
					continue;
				}
			}
			// 四条
			if (result.getType() <= 7) {
				Result tmp = four(cards);

				if (tmp != null) {
					if (result.getType() < 7)
						result = tmp;
					else if (result.getType() == 7 && result.compareTo(tmp) < 0)
						result = tmp;
					continue;
				}
			}
			// 葫芦
			if (result.getType() <= 6) {
				Result tmp = fullHouse(cards);
				if (tmp != null) {
					if (result.getType() < 6)
						result = tmp;
					else if (result.getType() == 6 && result.compareTo(tmp) < 0)
						result = tmp;
					continue;
				}
			}
			// 同花
			if (result.getType() < 5) {
				Result tmp = suit(cards);
				if (tmp != null) {
					if (result.getType() < 5)
						result = tmp;
					else if (result.getType() == 5 && result.compareTo(tmp) < 0)
						result = tmp;
					continue;
				}
			}
			// 顺子
			if (result.getType() < 4) {
				Result tmp = serial(cards);
				if (tmp != null) {
					if (result.getType() < 4)
						result = tmp;
					else if (result.getType() == 4 && result.compareTo(tmp) < 0)
						result = tmp;
					continue;
				}
			}
			// 三条
			if (result.getType() < 3) {
				Result tmp = three(cards);

				if (tmp != null) {
					if (result.getType() < 3)
						result = tmp;
					else if (result.getType() == 3 && result.compareTo(tmp) < 0)
						result = tmp;
					continue;
				}
			}
			// 两对
			if (result.getType() < 2) {
				Result tmp = pair2(cards);
				if (tmp != null) {
					if (result.getType() < 2)
						result = tmp;
					else if (result.getType() == 2 && result.compareTo(tmp) < 0)
						result = tmp;
					continue;
				}
			}
			// 对子
			if (result.getType() < 1) {
				Result tmp = pair(cards);
				if (tmp != null) {
					if (result.getType() < 1)
						result = tmp;
					else if (result.getType() == 1 && result.compareTo(tmp) < 0)
						result = tmp;
					continue;
				}
			}
			// 高牌
			if (result.getType() < 0) {
				Result tmp = high(cards);
				if (tmp != null) {
					if (result.getType() < 1)
						result = tmp;
					else if (result.getType() == 1 && result.compareTo(tmp) < 0)
						result = tmp;
					continue;
				}
			}
		}
		return result;
	}

	/**
	 * 组合操作，从7张中任取5张牌，组成21种情况
	 * 
	 * @param map
	 * @return
	 */
	private static Map<Integer, List<Card>> sevenToFive(List<Card> map) {
		Map<Integer, List<Card>> group = new HashMap<Integer, List<Card>>();
		int num = 0;
		for (int a = 0; a < 3; a++) {
			for (int b = a + 1; b < 4; b++) {
				for (int c = b + 1; c < 5; c++) {
					for (int d = c + 1; d < 6; d++) {
						for (int e = d + 1; e < 7; e++) {
							List<Card> pokerGroup = new ArrayList<Card>();
							pokerGroup.add(map.get(a));
							pokerGroup.add(map.get(b));
							pokerGroup.add(map.get(c));
							pokerGroup.add(map.get(d));
							pokerGroup.add(map.get(e));
							group.put(num++, pokerGroup);
						}
					}
				}
			}
		}
		num = 0;
		return group;
	}

	/**
	 * 计算牌型,该方法可以判断高牌、对子、两对、三条、葫芦和四条
	 * 
	 * @param pokers
	 * @return
	 */
	private static int pokerType11(List<Card> cards) {
		if (cards.size() != 5)
			return -1;
		int flag = 0;
		for (int i = 0; i < cards.size(); i++) {
			for (int j = i + 1; j < cards.size(); j++) {
				if (Card.code(cards.get(i).getSerial()) == Card.code(cards.get(j).getSerial()))
					flag++;
			}
		}
		switch (flag) {
		case 6:
			return 7; // 四条
		case 4:
			return 6; // 葫芦
		case 3:
			return 3; // 三条
		case 2:
			return 2; // 两对
		case 1:
			return 1; // 对子
		case 0:
			return 0; // 高牌
		default:
			return -1;
		}
	}

	/**
	 * 计算高牌
	 * 
	 * @param cards
	 * @return
	 */
	private static Result high(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);
		List<Frequency> frcs = Frequency.repetition(cards);
		if (frcs.size() == 0)
			return new Result(0, cards);
		else
			return null;
	}

	/**
	 * 计算对子
	 * 
	 * @param cards
	 * @return
	 */
	private static Result pair(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);
		List<Frequency> frcs = Frequency.repetition(cards);
		if (frcs.size() == 1 && frcs.get(0).getTime() == 2) {
			Card card = new Card(frcs.get(0).getCode() * Card.SUIT_NUM);
			cards.remove(card);
			cards.remove(card);
			return new Result(1, new Card(frcs.get(0).getCode() * Card.SUIT_NUM), cards);
		} else
			return null;
	}

	/**
	 * 计算两对
	 * 
	 * @param cards
	 * @return
	 */
	private static Result pair2(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);
		List<Frequency> frcs = Frequency.repetition(cards);
		if (frcs.size() == 2 && frcs.get(0).getTime() == 2 && frcs.get(1).getTime() == 2) {
			Card card = new Card(frcs.get(0).getCode() * Card.SUIT_NUM);
			cards.remove(card);
			cards.remove(card);
			Card card1 = new Card(frcs.get(1).getCode() * Card.SUIT_NUM);
			cards.remove(card1);
			cards.remove(card1);
			return new Result(2, new Card(frcs.get(0).getCode() * Card.SUIT_NUM),
					new Card(frcs.get(1).getCode() * Card.SUIT_NUM), cards);
		} else
			return null;
	}

	/**
	 * 计算三条
	 * 
	 * @param cards
	 * @return
	 */
	private static Result three(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);
		List<Frequency> frcs = Frequency.repetition(cards);
		if (frcs.size() == 1 && frcs.get(0).getTime() == 3) {
			Card card = new Card(frcs.get(0).getCode() * Card.SUIT_NUM);
			cards.remove(card);
			cards.remove(card);
			cards.remove(card);
			return new Result(3, new Card(frcs.get(0).getCode() * Card.SUIT_NUM), cards);
		} else
			return null;
	}

	/**
	 * 计算葫芦
	 * 
	 * @param cards
	 * @return
	 */
	private static Result fullHouse(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		List<Frequency> frcs = Frequency.repetition(cards);
		if (frcs.size() == 2 && (frcs.get(0).getTime() == 2 && frcs.get(1).getTime() == 3))
			return new Result(6, new Card(frcs.get(1).getCode() * Card.SUIT_NUM),
					new Card(frcs.get(0).getCode() * Card.SUIT_NUM));
		else if (frcs.size() == 2 && (frcs.get(0).getTime() == 3 && frcs.get(1).getTime() == 2))
			return new Result(6, new Card(frcs.get(0).getCode() * Card.SUIT_NUM),
					new Card(frcs.get(1).getCode() * Card.SUIT_NUM));
		else
			return null;
	}

	/**
	 * 计算四条
	 * 
	 * @param cards
	 * @return
	 */
	private static Result four(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);
		List<Frequency> frcs = Frequency.repetition(cards);
		if (frcs.size() == 1 && frcs.get(0).getTime() == 4) {
			Card card = new Card(frcs.get(0).getCode() * Card.SUIT_NUM);
			cards.remove(card);
			cards.remove(card);
			cards.remove(card);
			cards.remove(card);
			return new Result(7, new Card(frcs.get(0).getCode() * Card.SUIT_NUM), cards);
		} else
			return null;
	}

	/**
	 * 计算重复项得分
	 * 
	 * @param cards
	 * @return
	 */
	private static int flag(List<Card> cards) {
		if (cards.size() != 5)
			return -1;
		int flag = 0;
		for (int i = 0; i < cards.size(); i++) {
			for (int j = i + 1; j < cards.size(); j++) {
				if (Card.code(cards.get(i).getSerial()) == Card.code(cards.get(j).getSerial()))
					flag++;
			}
		}
		return flag;
	}

	/**
	 * 判断5张牌是否同花
	 * 
	 * @param cards
	 * @return 5
	 */
	private static Result suit(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);// 先按照牌面从小到大排序
		boolean flag = false;
		for (int i = 0; i < cards.size() - 1; i++) {
			if (Card.suit(cards.get(i).getSerial()) == Card.suit(cards.get(i + 1).getSerial())) {
				flag = true;
				continue;
			} else {
				flag = false;
				break;
			}
		}
		if (flag)
			return new Result(5, cards);
		else
			return null;
	}

	/**
	 * 判断顺子
	 * 
	 * @param cards
	 * @return
	 */
	private static Result serial(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);// 先按照牌面从小到大排序
		boolean flag = false;
		for (int i = 4; i > 0; i--) {
			if (Card.code(cards.get(i).getSerial()) - Card.code(cards.get(i - 1).getSerial()) == 1)
				flag = true;
			else {
				flag = false;
				break;
			}
		}
		if (flag)
			return new Result(4, new Card(cards.get(4).getSerial()));
		else
			return serial_poor(cards);
	}

	/**
	 * 判断顺子是不是最小的《1,2,3,4,5》
	 * 
	 * @param cards
	 * @return
	 */
	private static Result serial_poor(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);// 先按照牌面从小到大排序
		boolean flag = false;
		for (int i = 0; i < cards.size() - 1; i++) {
			if (Card.code(cards.get(i).getSerial()) == i + 1)
				flag = true;
			else {
				flag = false;
				break;
			}
		}
		if (flag && Card.code(cards.get(4).getSerial()) == 12)
			return new Result(4, new Card(cards.get(3).getSerial()));
		else
			return null;
	}

	/**
	 * 判断同花顺
	 * 
	 * @param pokers
	 * @return
	 */
	private static Result suitSerial(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);// 先按照牌面从小到大排序
		if (serial(cards) != null && suit(cards) != null)
			return new Result(8, new Card(cards.get(4).getSerial()));
		else
			return null;
	}

	/**
	 * 判断皇家同花顺
	 * 
	 * @param pokers
	 * @return
	 */
	private static Result king(List<Card> cards) {
		if (cards.size() != 5)
			return null;
		Collections.sort(cards);// 先按照牌面从小到大排序
		if (suitSerial(cards) != null && Card.code(cards.get(4).getSerial()) == 12)
			return new Result(9);
		else
			return null;
	}

	/**
	 * 计算当前牌的得分，用于比较相同类型牌的大小
	 * 
	 * @param cards
	 * @param level
	 * @return
	 */
	private static long score(List<Card> cards, int level) {
		long lon = 1000000000L * 10;
		long pkValue = 0L;
		for (int i = 0; i < cards.size(); i++) {
			int interval = 1;
			for (int j = i; j < cards.size() - 1; j++) {
				interval *= 10;
				interval *= 10;
			}
			pkValue += Card.code(cards.get(i).getSerial()) * interval;
		}
		pkValue += level * lon;
		return pkValue;
	}
}
