package com.pptv.calculator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.pptv.holdem.Card;
import com.pptv.holdem.Result;

/**
 * 计算胜负关系，手牌牌型和赢牌牌型
 * @author longhu
 *
 */
public class Calculator {

	public static final String[] result = { "牛仔胜", "平手", "小牛胜" };

	public static final String[] winType = { "高牌", "一对", "两对", "三条", "顺子", "同花", "葫芦", "四条", "同花顺", "皇家同花顺" };

	public static final String[] handType = { "同花", "连牌", "对子", "同花连牌", "对A" };

	/**
	 * 比较牛仔和小牛胜负关系
	 * 
	 * @param cowboy
	 * @param calf
	 * @return
	 */
	public static String whoWin(Result cowboy, Result calf) {
		return result[calf.compareTo(cowboy) + 1];
	}

	/**
	 * 获胜牌型
	 * 
	 * @param cowboy
	 * @param calf
	 * @return
	 */
	public static String winType(Result cowboy, Result calf) {
		if (cowboy.compareTo(calf) >= 0)
			return winType[cowboy.getType()];
		else
			return winType[calf.getType()];
	}

	/**
	 * 手牌牌型
	 * @param cowBoy
	 * @param calf
	 * @return
	 */
	public static Set<String> handType(List<Card> cowboyHand,List<Card> calfHand) {
		Set<String> set = new HashSet<String>();
		if(HandTypeCal.isSuit(cowboyHand, calfHand))
			set.add(handType[0]);
		if(HandTypeCal.isSerial(cowboyHand, calfHand))
			set.add(handType[1]);
		if(HandTypeCal.isPair(cowboyHand, calfHand))
			set.add(handType[2]);
		if(HandTypeCal.isSuitSerial(cowboyHand, calfHand))
			set.add(handType[3]);
		if(HandTypeCal.isDoubleA(cowboyHand, calfHand))
			set.add(handType[4]);
		return set;
	}
}
