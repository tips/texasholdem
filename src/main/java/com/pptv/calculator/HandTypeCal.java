package com.pptv.calculator;

import java.util.List;

import com.pptv.holdem.Card;

public class HandTypeCal implements CalculatorInterface {

	/**
	 * 判断任一人手牌是不是同花
	 * 
	 * @param cowBoy
	 * @param calf
	 * @return
	 */
	public static boolean isSuit(List<Card> cowBoy, List<Card> calf) {
		return suit(cowBoy) || suit(calf);
	}

	/**
	 * 判断是否同花
	 * 
	 * @param list
	 * @return
	 */
	private static boolean suit(List<Card> list) {
		return Card.suit(list.get(0).getSerial()) == Card.suit(list.get(1).getSerial());
	}

	/**
	 * 判断任一人手牌是不是连牌
	 * 
	 * @param cowBoy
	 * @param calf
	 * @return
	 */
	public static boolean isSerial(List<Card> cowBoy, List<Card> calf) {
		return serial(cowBoy) || serial(calf);
	}

	/**
	 * 判断是否顺子
	 * 
	 * @param list
	 * @return
	 */
	private static boolean serial(List<Card> list) {
		return Math.abs((Card.code(list.get(0).getSerial()) - Card.code(list.get(1).getSerial()))) == 1;
	}

	/**
	 * 判断任一人手牌是不是对子
	 * 
	 * @param cowBoy
	 * @param calf
	 * @return
	 */
	public static boolean isPair(List<Card> cowBoy, List<Card> calf) {
		return pair(cowBoy) || pair(calf);
	}

	/**
	 * 判断是否是对子
	 * 
	 * @param cowBoy
	 * @param calf
	 * @return
	 */
	private static boolean pair(List<Card> list) {
		return Card.code(list.get(0).getSerial()) == Card.code(list.get(1).getSerial());
	}

	/**
	 * 判断任一人手牌是不是同花连牌
	 * 
	 * @param cowBoy
	 * @param calf
	 * @return
	 */
	public static boolean isSuitSerial(List<Card> cowBoy, List<Card> calf) {
		return suitSerial(cowBoy) || suitSerial(calf);
	}

	/**
	 * 判断是否同花连牌
	 * 
	 * @return
	 */
	private static boolean suitSerial(List<Card> list) {
		return suit(list) && serial(list);
	}

	/**
	 * 判断任一人手牌是否对A
	 * 
	 * @param list
	 * @return
	 */
	public static boolean isDoubleA(List<Card> cowBoy, List<Card> calf) {
		return doubleA(cowBoy) || doubleA(calf);
	}

	/**
	 * 判断是否对A
	 * 
	 * @param list
	 * @return
	 */
	private static boolean doubleA(List<Card> list) {
		return pair(list) && Card.code(list.get(0).getSerial()) == 12;
	}
}
