package com.pptv.utils;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.pptv.analysis.Record;

public class DBUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(DBUtil.class);
	
	private static Connection getConn() {
	    String driver = "com.mysql.jdbc.Driver";
	    String url = "jdbc:mysql://10.200.11.186:3306/texasholdem?characterEncoding=utf-8";
	    String username = "pplive";
	    String password = "123456";
	    Connection conn = null;
	    try {
	        Class.forName(driver);
	        conn = (Connection) DriverManager.getConnection(url, username, password);
	    } catch (ClassNotFoundException e) {
	    	logger.error("加载驱动失败：{}",e.getMessage());
	    } catch (SQLException e) {
	    	logger.error("数据库连接失败：{}",e.getMessage());
	    }
	    return conn;
	}
	
	public static int insert(Record record) {
	    Connection conn = getConn();
	    int i = 0;
	    String sql = "insert into record (board,cowboy,calf,handtype,wintype,who,time) values(?,?,?,?,?,?,?)";
	    PreparedStatement pstmt;
	    try {
	        pstmt = (PreparedStatement) conn.prepareStatement(sql);
	        pstmt.setString(1, record.getBoard());
	        pstmt.setString(2, record.getCowboy());
	        pstmt.setString(3, record.getCalf());
	        pstmt.setString(4, record.getHandtype());
	        pstmt.setString(5, record.getWintype());
	        pstmt.setString(6, record.getWho());
	        pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
	        i = pstmt.executeUpdate();
	        pstmt.close();
	        conn.close();
	    } catch (SQLException e) {
	    	logger.error("插入数据异常：{}",e.getMessage());
	    }
	    return i;
	}
}
